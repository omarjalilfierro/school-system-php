<?php
namespace peopleapp\afficheur;

abstract class AfficheurPersonne {

    abstract public function vueCourte(); 

    abstract public function vueDetaille();
}