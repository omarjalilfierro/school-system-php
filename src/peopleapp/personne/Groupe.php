<?php
namespace peopleapp\personne;


class Groupe{

    private $groupe, $semestre, $formation, $liste=array();

    public function __construct($groupe, $semestre, $formation){
        $this -> groupe = $groupe;
        $this -> formation = $formation;
        $this->semestre = $semestre;
    }

    public function ajouterEtudiant($etudiant){
        $this->liste[] = $etudiant;
    }

    public function calculerMoyenneGroupeMat($matiere){
        $tot = 0;
        $i = 0;
        foreach ($this -> liste as $item){
            $tot += $item -> calculerMoyenneMat($matiere);
            $i++;
        }
        return $tot/$i;
    }

    public function calculerMoyenneGroupe($tri){
        $moy = array();
        foreach ($this->liste as $item){
            $moy[$item -> nom] = $item -> moyenneGeneral;
        }
        if ($tri === "noms") {
            ksort($moy);
            return $moy;
        }
        if ($tri === "notes"){
            asort($moy);
            return $moy;
        }
        return null;
    }
}