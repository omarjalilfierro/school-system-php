<?php
namespace peopleapp\personne;

class Etudiant extends Personne {

    private $numEtudiant, $refFormation, $groupe, $notes=array(),$moyenneGeneral;


    public function ecrirePunition($phrase, $nombre){
        for ($i=0; $i < $nombre; $i++) { 
            echo $phrase;
        }
    }

    public function ajouterNote($matiere,$note){
        $this -> notes[$matiere][] = $note;
    }

    public function ajouterNotes($matiere,$note){
        foreach (explode(";", $note) as $item) {
            $this->ajouterNote($matiere, (float)$item);
        }
    }

    public function getArray(){
        print_r($this -> notes);
    }

    public function calculerMoyenneGenerale(){
        $moyenne = array();
        $tot = 0;
        $i = 0;
        foreach ($this -> notes as $matiere => $notes) {
            $moyenne_matiere = $this->calculerMoyenneMat($matiere);
            $moyenne[$matiere] = $moyenne_matiere;
            $tot += $moyenne_matiere;
            $i++;
        }
        $this->moyenneGeneral = $tot/$i;
        $moyenne["Generale"] = $this->moyenneGeneral;
        return $moyenne;
    }

    public function calculerMoyenneMat($matiere){
        $i = 0;
        $tot = 0;
        if (array_key_exists($matiere,$this -> notes)){
            foreach ($this->notes[$matiere] as $matiere => $v) {
                    $tot += $v;
                    $i++;
            }
            return $tot/$i;
        }
        else
            throw new \Exception('Key doesnt exists');
    }



    function isIterable($var)
    {
        return $var !== null
            && (is_array($var)
                || $var instanceof Traversable
                || $var instanceof Iterator
                || $var instanceof IteratorAggregate
            );
    }

    public function __set($att, $val){
        if (property_exists)
            $this -> $att = $val;
        else throw new Exception("Nope");    
    }

    public function __get($att){
        if (property_exists)
            return $this -> $att;
        else throw new Exception("Nope");    
    }
}